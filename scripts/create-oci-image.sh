#!/bin/bash

set -euxo pipefail

set_vars() {
    DISTRO="${DISTRO:-autosd}"
    MANIFEST_DIR="centos-boot"
    MANIFEST_FILE="centos-sig-autosd-minimal.yaml"
}

build_oci_image() {
    local distro="${1}"

    CMD=("rpm-ostree" \
         "compose" \
         "image" \
         "--initialize" \
         "--format=ociarchive" \
         "${MANIFEST_FILE}" \
         "${DISTRO}-minimal.oci-archive")
    "${CMD[@]}"
}

set_vars

cd "${MANIFEST_DIR}"

echo "[+] Building OCI image for ${DISTRO}"
build_oci_image "${DISTRO}"

echo "[+] Moving the generated OCI image"
mkdir -p "${DOWNLOAD_DIRECTORY}"
mv "${DISTRO}-minimal.oci-archive" "${DOWNLOAD_DIRECTORY}/"
