#!/bin/bash

set -euxo pipefail

# Login to registry
skopeo login --username "${CONTAINER_REGISTRY_USER}" --password "${CONTAINER_REGISTRY_PASSWORD}" "${CONTAINER_REGISTRY}" 

# Push the generated OCI image to registry
skopeo copy \
    oci-archive:"${DOWNLOAD_DIRECTORY}/${DISTRO}"-minimal.oci-archive \
    docker://"${CONTAINER_REGISTRY}/${CONTAINER_REGISTRY_NAMESPACE}/${OCI_IMAGE_NAME}:${OCI_IMAGE_TAG}"
