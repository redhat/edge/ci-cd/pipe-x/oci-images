#!/bin/bash

set -euxo pipefail

clone_images_repo() {
  local VARIABLES_COUNT=$#
  if [ ${VARIABLES_COUNT} -eq 3 ]; then
    local repo_url="$1"
    local repo_dir="$2"
    local revision="$3"
  else
    echo -n "Expecting 3 variables defined but got ${VARIABLES_COUNT}. Exiting" >&2
    return 1
  fi

  # Repeat the clone until 5 times and waits 10 seconds in between if it is failed
  local n=0
  until [[ $n -ge 5 ]]
  do
    git clone "${repo_url}" "${repo_dir}" && break || {
      ((n++))
      sleep 10
    }
  done
  pushd "${repo_dir}" || exit 1
  if git ls-remote --tags origin "${revision}" | grep -q "${revision}"; then
    git checkout -b upstream "${revision}"
  else
    git checkout -b upstream "origin/${revision}"
  fi
  popd || exit 1
}

BOOTC_DIR="${BOOTC_DIR:-centos-boot}"

echo "[+] Remove any previous mpp files"
rm -vfr "${BOOTC_DIR}"

echo "[+] Clone the repository with the mpp files"
clone_images_repo "$REPO_URL" "$BOOTC_DIR" "$REVISION"
echo "[+] Cloned"
